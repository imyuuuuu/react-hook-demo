import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import React from 'react';

function ProductCard({productDetail}) {
  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={productDetail.image} />
      <Card.Body>
        <Card.Title>{productDetail.name}</Card.Title>
        <Card.Text>
          {productDetail.describe}
        </Card.Text>
        <Button variant="primary">View Details</Button>
        <Button variant="danger ms-2">Price : {productDetail.price}</Button>
        
      </Card.Body>
    </Card>
  );
}

export default ProductCard;