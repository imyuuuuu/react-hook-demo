import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { useState } from 'react';

function PokimonCard() {
    let PokimonDate = [
        {
            name : "Charmeleon",
            describe :" CharmeleonStats HP Attack Defense Special Attack     Special Defense Speed It has a barbaric nature. In battle, it whips its fiery tail around and slashes away with sharp claws. ",
            image : "https://assets.pokemon.com/assets/cms2/img/pokedex/full/005.png"
        },
        {
            name : "Charizard",
            describe :"It is said that Charizard fire burns hotter if it has experienced harsh battles.",
            image : "https://assets.pokemon.com/assets/cms2/img/pokedex/full/006.png"
        },
        {
            name : "Squirtle",
            describe :"When it retracts its long neck into its shell, it squirts out water with vigorous force.",
            image : "https://assets.pokemon.com/assets/cms2/img/pokedex/full/007.png"
        },
    ]

    const [data, setData] = useState(PokimonDate[0])
    const [counter, setCounter] = useState(1)

    const handleTranform = () => {
        setCounter((counter >= PokimonDate.length - 1)? 0 : counter + 1)
        setData(PokimonDate[counter])
    }

  return (
    <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src={data.image} />
      <Card.Body>
        <Card.Title>{data.name}</Card.Title>
        <Card.Text>
            {data.describe}
        </Card.Text>
        <Button variant="danger" onClick={()=> handleTranform()}>Tranform</Button>
      </Card.Body>
    </Card>
  );
}

export default PokimonCard;