import React, { useState } from 'react'
import ProductCard from '../components/ProductCard'

const ProductPage = ({productData}) => {
    const [product, setProduct] = useState(productData)

  return (
    <div className='container'>
        <div className="row">
            {productData.map((product) => (
                <div className="col-4">
                <ProductCard productDetail = {product}/>
            </div>
            ))}
        </div>
    </div>
  )
}

export default ProductPage