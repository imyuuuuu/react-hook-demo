import React, { useState } from 'react'

const StateCounter = () => {
    const [counter, setCounter] = useState(0)

    const handleIncrease=()=> {
    setCounter(counter + 1)
    }
    const handleDecrease=()=> {
    setCounter(counter - 1)
    }
  return (
    <div className='text-center'>
        <h1 className='text-center'>{counter}</h1>
        <button className='btn btn-warning' onClick={()=> handleIncrease()}>Increase</button>
        <button className='btn btn-danger' onClick={()=> handleDecrease()}>Decrease</button>
    </div>
  )
}

export default StateCounter