import React, { useState } from 'react'
import PokimonCard from '../components/PokimonCard'

const PokimonPage = () => {

  return (
    <div className='d-flex justify-content-center mt-5'>
        <PokimonCard/>
    </div>
  )
}

export default PokimonPage