import PokimonPage from './pages/PokimonPage';
import ProductPage from './pages/ProductPage';
import StateCounter from './pages/StateCounter';
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {

  let drinkProducts = [
    {
      name : 'CocaCola',
      price : "2.5$",
      describe : "On May 8, 1886, Dr. John Pemberton sold the first glass of CocaCola at Jacobs' Pharmacy in downtown Atlanta. Serving nine drinks per day in its first year.",
      image : "https://api.freelogodesign.org/assets/blog/thumb/f4dae7732213491da3952f853c48f6dc_1176x840.jpg?t=638368678710000000"
    },
    {
      name : 'Fanta',
      price : "1.5$",
      describe : "There are more than 200 flavors worldwide. Fanta originated in Germany as a Coca-Cola alternative in 1940 due to the American trade embargo of Nazi Germany.",
      image : "https://www.coca-cola.com/content/dam/onexp/us/en/brands/fanta/Fanta-Desktop-Header-1440x810.png"
    },
    {
      name : 'Sting',
      price : "2$",
      describe : "High Caffeine Intake: Energy drinks like Sting  typically contain high levels of caffeine, which can lead to increased heart rate, elevated blood pressure, and disrupted sleep",
      image : "https://miro.medium.com/v2/resize:fit:808/0*iTuZqoKKos3CgBU5.jpg"
    },
    {
      name : 'Sprite',
      price : "2.5$",
      describe : "The OG, the flavor that started it all—classic, cool, crisp lemon-lime taste that's caffeine free with 100% natural flavors",
      image : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIA7ND7jlGEKui6PsrM6VM2hODOzpeHmqE5g&usqp=CAU"
    },
  ]
  return (
    <>
      {/* <StateCounter/> */}
      {/* <PokimonPage/> */}
      <ProductPage productData = {drinkProducts}/>
    </>
  );
}

export default App;
